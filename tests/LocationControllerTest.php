<?php

class LocationControllerTest extends TestCase
{
    public function testGetProvinces()
    {
        $response = $this->call('GET', '/v1/locations/provinces');
        $data = json_decode($response->getContent());

        $this->assertArrayHasKey('error', (array)$data);
        $this->assertArrayHasKey('message', (array)$data);
        $this->assertArrayHasKey('provinces', (array)$data);
        $this->assertEquals(200, $response->status());
        $this->assertEquals(false, $data->error);
        $this->assertEquals('Successfully get list of provinces.', $data->message);
    }

    public function testGetCities()
    {
        $params = [
            'province_id' => 1,
        ];
        $response = $this->call('GET', '/v1/locations/cities', $params);
        $data = json_decode($response->getContent());

        $this->assertArrayHasKey('error', (array)$data);
        $this->assertArrayHasKey('message', (array)$data);
        $this->assertArrayHasKey('cities', (array)$data);
        $this->assertEquals(200, $response->status());
        $this->assertEquals(false, $data->error);
        $this->assertEquals('Successfully get list of cities.', $data->message);
    }

    public function testGetCitiesWithNonExistProvince()
    {
        $params = [
            'province_id' => 0,
        ];
        $response = $this->call('GET', '/v1/locations/cities', $params);
        $data = json_decode($response->getContent());

        $this->assertArrayHasKey('error', (array)$data);
        $this->assertArrayHasKey('message', (array)$data);
        $this->assertEquals(200, $response->status());
        $this->assertEquals(true, $data->error);
        $this->assertEquals('Result empty.', $data->message);
    }

    public function testGetDistricts()
    {
        $params = [
            'city_id' => 1,
        ];
        $response = $this->call('GET', '/v1/locations/districts', $params);
        $data = json_decode($response->getContent());

        $this->assertArrayHasKey('error', (array)$data);
        $this->assertArrayHasKey('message', (array)$data);
        $this->assertArrayHasKey('districts', (array)$data);
        $this->assertEquals(200, $response->status());
        $this->assertEquals(false, $data->error);
        $this->assertEquals('Successfully get list of districts.', $data->message);
    }

    public function testGetDistrictsWithNonExistCity()
    {
        $params = [
            'city_id' => 0,
        ];
        $response = $this->call('GET', '/v1/locations/districts', $params);
        $data = json_decode($response->getContent());

        $this->assertArrayHasKey('error', (array)$data);
        $this->assertArrayHasKey('message', (array)$data);
        $this->assertEquals(200, $response->status());
        $this->assertEquals(true, $data->error);
        $this->assertEquals('Result empty.', $data->message);
    }

    public function testGetSubdistricts()
    {
        $params = [
            'district_id' => 1,
        ];
        $response = $this->call('GET', '/v1/locations/subdistricts', $params);
        $data = json_decode($response->getContent());

        $this->assertArrayHasKey('error', (array)$data);
        $this->assertArrayHasKey('message', (array)$data);
        $this->assertArrayHasKey('subdistricts', (array)$data);
        $this->assertEquals(200, $response->status());
        $this->assertEquals(false, $data->error);
        $this->assertEquals('Successfully get list of subdistricts.', $data->message);
    }

    public function testGetSubdistrictsWithNonExistDistrict()
    {
        $params = [
            'district_id' => 0,
        ];
        $response = $this->call('GET', '/v1/locations/subdistricts', $params);
        $data = json_decode($response->getContent());

        $this->assertArrayHasKey('error', (array)$data);
        $this->assertArrayHasKey('message', (array)$data);
        $this->assertEquals(200, $response->status());
        $this->assertEquals(true, $data->error);
        $this->assertEquals('Result empty.', $data->message);
    }

    public function testGetSubdistrictsWithoutDistrictId()
    {
        $params = [
            'limit' => 2,
        ];
        $response = $this->call('GET', '/v1/locations/subdistricts', $params);
        $data = json_decode($response->getContent());

        $this->assertArrayHasKey('error', (array)$data);
        $this->assertArrayHasKey('message', (array)$data);
        $this->assertArrayHasKey('subdistricts', (array)$data);
        $this->assertEquals(200, $response->status());
        $this->assertEquals(false, $data->error);
        $this->assertEquals('Successfully get list of subdistricts.', $data->message);
        $this->assertEquals(2, count($data->subdistricts));
    }
}