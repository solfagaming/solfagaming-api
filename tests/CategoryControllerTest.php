<?php

class CategoryControllerTest extends TestCase
{
    /**
     * Register a new admin.
     *
     * @return void
     */
    public function registerAdmin()
    {
        DB::table('users')->insert([
            'name' => 'New Admin',
            'email' => 'admin@mail.com',
            'password' => app('hash')->make('password'),
            'role' => 1,
            'state' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
    }

    /**
     * Logs in a registered admin.
     *
     * @return String access_token
     */
    public function login()
    {
        $this->registerAdmin();
        $params = [
            'grant_type' => 'password',
            'client_id' => 2,
            'client_secret' => 'jtENSd6fEJ5k9PFoGHTfWHc5Kqn8i8IzG4fp1DHm',
            'username' => 'admin@mail.com',
            'password' => 'password',
        ];
        $response = $this->call('POST', '/v1/oauth/token', $params);
        $data = json_decode($response->getContent());
        return $data->access_token;
    }

    /**
     * Add new categories.
     *
     * @return string
     */
    public function addCategories()
    {
        $token = $this->login();
        $cat1 = factory(App\Category::class)->create([
            'name' => 'Test category 1'
        ]);

        $subcat1 = factory(App\Category::class)->create([
            'parent_id' => $cat1->category_id,
            'name' => 'Test sub category 1'
        ]);

        $cat2 = factory(App\Category::class)->create([
            'name' => 'Test category 2'
        ]);

        $subcat2 = factory(App\Category::class)->create([
            'parent_id' => $cat2->category_id,
            'name' => 'Test sub category 2'
        ]);

        return $token;
    }

    /**
     * Get all categories.
     *
     * @return array
     */
    public function getAllCategories()
    {
        $token = $this->addCategories();
        $server = $this->transformHeadersToServerVars(['Authorization' => 'Bearer ' . $token]);

        $response = $this->call('GET', '/v1/admins/categories', [], [], [], $server);
        $data = json_decode($response->getContent());

        $result['token'] = $token;
        $result['categories'] = $data->categories;

        return $result;
    }

    /**
     * Test adding a new category.
     *
     * @return void
     */
    public function testAddCategory()
    {
        $token = $this->login();
        $server = $this->transformHeadersToServerVars(['Authorization' => 'Bearer ' . $token]);

        $params = [
            'name' => 'Test Category',
            'description' => 'A test category.',
            'logo_url' => 'http://placehold.it/350x350',
        ];

        $response = $this->call('POST', '/v1/admins/categories/add', $params, [], [], $server);
        $data = json_decode($response->getContent());

        $this->assertEquals(201, $response->status());
        $this->assertEquals(false, $data->error);
        $this->assertEquals('Category has been created.', $data->message);
        $this->seeInDatabase('categories', ['name' => 'Test Category', 'slug' => 'test-category', 'description' => 'A test category.']);
    }

    /**
     * Test adding a new child category.
     *
     * @return void
     */
    public function testAddChildCategory()
    {
        $data = $this->getAllCategories();
        $token = $data['token'];
        $categories = $data['categories'];
        $server = $this->transformHeadersToServerVars(['Authorization' => 'Bearer ' . $token]);

        $params = [
            'parent_id' => $categories[0]->category_id,
            'name' => 'Test Child Category',
            'description' => 'A test child category.',
            'logo_url' => 'http://placehold.it/350x350',
        ];

        $response = $this->call('POST', '/v1/admins/categories/add', $params, [], [], $server);
        $data = json_decode($response->getContent());

        $this->assertEquals(201, $response->status());
        $this->assertEquals(false, $data->error);
        $this->assertEquals('Category has been created.', $data->message);
        $this->seeInDatabase('categories', ['parent_id' => $categories[0]->category_id, 'name' => 'Test Child Category', 'slug' => 'test-child-category', 'description' => 'A test child category.']);
    }

    /**
     * Test getting all active categories.
     *
     * @return int
     */
    public function testGetAllActiveCategories()
    {
        $token = $this->addCategories();
        $server = $this->transformHeadersToServerVars(['Authorization' => 'Bearer ' . $token]);

        $response = $this->call('GET', '/v1/admins/categories', [], [], [], $server);
        $data = json_decode($response->getContent());

        $this->assertEquals(200, $response->status());
        $this->assertEquals(false, $data->error);
        $this->assertEquals('Successfully get all categories.', $data->message);
        $this->assertEquals(2, count($data->categories));
    }

    /**
     * Test getting all categories, including pending and blocked.
     *
     * @return int
     */
    public function testGetAllCategories()
    {
        $token = $this->addCategories();
        $server = $this->transformHeadersToServerVars(['Authorization' => 'Bearer ' . $token]);

        $params = [
            'get_all_state' => 1,
        ];
        $response = $this->call('GET', '/v1/admins/categories', [], [], [], $server);
        $data = json_decode($response->getContent());

        $this->assertEquals(200, $response->status());
        $this->assertEquals(false, $data->error);
        $this->assertEquals('Successfully get all categories.', $data->message);
        $this->assertEquals(2, count($data->categories));
    }

    /**
     * Test get category by category ID.
     *
     * @return void
     */
    public function testGetCategoryById()
    {
        $data = $this->getAllCategories();
        $server = $this->transformHeadersToServerVars(['Authorization' => 'Bearer ' . $data['token']]);

        $params = [
            'category_id' => $data['categories'][0]->category_id
        ];

        $response = $this->call('POST', '/v1/admins/categories/search', $params, [], [], $server);
        $data = json_decode($response->getContent());

        $this->assertEquals(200, $response->status());
        $this->assertEquals(false, $data->error);
        $this->assertEquals('Successfully get list of categories.', $data->message);
        $this->assertEquals(1, count($data->categories));
    }

    /**
     * Test get category by category ID with products.
     *
     * @return void
     */
    public function testGetCategoryByIdWithProducts()
    {
        $categories = $this->getAllCategories();
        $server = $this->transformHeadersToServerVars(['Authorization' => 'Bearer ' . $categories['token']]);

        $params = [
            'category_id' => $categories['categories'][0]->category_id,
            'get_products' => 1
        ];

        $response = $this->call('POST', '/v1/admins/categories/search', $params, [], [], $server);
        $data = json_decode($response->getContent());

        $this->assertArrayHasKey('products', (array)$data->categories[0]);
        $this->assertEquals(200, $response->status());
        $this->assertEquals(false, $data->error);
        $this->assertEquals('Successfully get list of categories.', $data->message);
        $this->assertEquals(1, count($data->categories));
    }

    /**
     * Test searching a category.
     *
     * @return void
     */
    public function testSearchCategory()
    {
        $token = $this->addCategories();
        $server = $this->transformHeadersToServerVars(['Authorization' => 'Bearer ' . $token]);

        $params = [
            'q' => 'Test category 1',
        ];

        $response = $this->call('POST', '/v1/admins/categories/search', $params, [], [], $server);
        $data = json_decode($response->getContent());

        $this->assertEquals(200, $response->status());
        $this->assertEquals(false, $data->error);
        $this->assertEquals('Successfully get list of categories.', $data->message);
        $this->assertEquals(1, count($data->categories));
    }

    /**
     * Test searching a category with limit.
     *
     * @return void
     */
    public function testSearchCategoryWithLimit()
    {
        $token = $this->addCategories();
        $server = $this->transformHeadersToServerVars(['Authorization' => 'Bearer ' . $token]);

        $params = [
            'q' => 'test',
            'limit' => 1,
        ];

        $response = $this->call('POST', '/v1/admins/categories/search', $params, [], [], $server);
        $data = json_decode($response->getContent());

        $this->assertEquals(200, $response->status());
        $this->assertEquals(false, $data->error);
        $this->assertEquals('Successfully get list of categories.', $data->message);
        $this->assertEquals(1, count($data->categories));
    }

    /**
     * Test searching parent category and include attribute parent and child.
     *
     * @return void
     */
    public function testSearchParentCategoryWithParentAndChilden()
    {
        $token = $this->addCategories();
        $server = $this->transformHeadersToServerVars(['Authorization' => 'Bearer ' . $token]);

        $params = [
            'parent_id' => "0",
            'get_parent' => 1,
            'get_children' => 1
        ];

        $response = $this->call('POST', '/v1/admins/categories/search', $params, [], [], $server);
        $data = json_decode($response->getContent());

        $this->assertEquals(200, $response->status());
        $this->assertEquals(false, $data->error);
        $this->assertEquals('Successfully get list of categories.', $data->message);
        $this->assertEquals(2, count($data->categories));
    }

    /**
     * Test updating a category.
     *
     * @return void
     */
    public function testUpdateCategory()
    {
        $data = $this->getAllCategories();
        $server = $this->transformHeadersToServerVars(['Authorization' => 'Bearer ' . $data['token']]);

        $params = [
            'category_id' => $data['categories'][0]->category_id,
            'name' => 'Test Update Category',
            'description' => 'A test category updated.',
            'logo_url' => 'http://placehold.it/400x400',
        ];

        $response = $this->call('POST', '/v1/admins/categories/update', $params, [], [], $server);
        $data = json_decode($response->getContent());

        $this->assertEquals(200, $response->status());
        $this->assertEquals(false, $data->error);
        $this->assertEquals('Category has been updated.', $data->message);
        $this->seeInDatabase('categories', ['name' => 'Test Update Category', 'description' => 'A test category updated.']);
    }

    /**
     * Test updating a category with guest credentials.
     *
     * @return void
     */
    public function testUpdateCategoryWithGuestCredentials()
    {
        $cat1 = factory(App\Category::class)->create();
        $params = [
            'category_id' => $cat1->category_id,
            'name' => 'Test Update Category',
            'description' => 'A test category updated.',
            'logo_url' => 'http://placehold.it/400x400',
        ];

        $response = $this->call('POST', '/v1/admins/categories/update', $params);
        $data = json_decode($response->getContent());

        $this->assertArrayHasKey('error', (array)$data);
        $this->assertArrayHasKey('message', (array)$data);
        $this->assertEquals(401, $response->status());
        $this->assertEquals(true, $data->error);
        $this->assertEquals('Unauthorized.', $data->message);
    }

    /**
     * Test updating a category with wrong ID.
     *
     * @return void
     */
    public function testUpdateCategoryWithWrongId()
    {
        $data = $this->getAllCategories();
        $server = $this->transformHeadersToServerVars(['Authorization' => 'Bearer ' . $data['token']]);

        $params = [
            'category_id' => 'xxx',
            'name' => 'Test Update Category',
            'description' => 'A test category updated.',
        ];

        $response = $this->call('POST', '/v1/admins/categories/update', $params, [], [], $server);
        $data = json_decode($response->getContent());

        $this->assertEquals(404, $response->status());
        $this->assertEquals(true, $data->error);
        $this->assertEquals('Category not found.', $data->message);
    }

    /**
     * Test deleting a category.
     *
     * @return void
     */
    public function testDeleteCategory()
    {
        $data = $this->getAllCategories();
        $categoryId = $data['categories'][0]->category_id;
        $server = $this->transformHeadersToServerVars(['Authorization' => 'Bearer ' . $data['token']]);

        $params = [
            'category_id' => $categoryId,
        ];

        $response = $this->call('POST', '/v1/admins/categories/delete', $params, [], [], $server);
        $data = json_decode($response->getContent());

        $this->assertEquals(200, $response->status());
        $this->assertEquals(false, $data->error);
        $this->assertEquals('Category has been deleted.', $data->message);
        $this->seeInDatabase('categories', ['category_id' => $categoryId, 'state' => 4]);
    }

    /**
     * Test deleting a category with wrong ID.
     *
     * @return void
     */
    public function testDeleteCategoryWithWrongId()
    {
        $token = $this->login();
        $server = $this->transformHeadersToServerVars(['Authorization' => 'Bearer ' . $token]);

        $params = [
            'category_id' => 0
        ];

        $response = $this->call('POST', '/v1/admins/categories/delete', $params, [], [], $server);
        $data = json_decode($response->getContent());

        $this->assertEquals(404, $response->status());
        $this->assertEquals(true, $data->error);
        $this->assertEquals('Category not found.', $data->message);
    }

    /**
     * Test delete category using customer credentials/token.
     *
     * @return void
     */
    public function testDeleteCategoryWithCustomerCredentials()
    {
        $categories = $this->getAllCategories();
        $customer = factory('App\User')->create([
            'role' => 2,
            'state' => 1
        ]);
        $this->actingAs($customer);
        $params = [
            'category_id' => $categories['categories'][0]->category_id,
        ];

        $response = $this->call('POST', '/v1/admins/categories/delete', $params);
        $data = json_decode($response->getContent());

        $this->assertArrayHasKey('error', (array)$data);
        $this->assertArrayHasKey('message', (array)$data);
        $this->assertEquals(401, $response->status());
        $this->assertEquals(true, $data->error);
        $this->assertEquals('Unauthorized access.', $data->message);
    }
}