<?php

namespace App;

use App\Http\Utilities\Constants;
use Laravel\Passport\HasApiTokens;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Support\Facades\Hash;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use HasApiTokens, Authenticatable, Authorizable;
    use \App\FbLoginTrait;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * Override passport username check, to check user state.
     *
     * @param string $username
     * @return \App\User
     */
    public function findForPassport($username)
    {
        return $this->where('email', $username)->where('state', Constants::STATE_ACTIVE)->first();
    }

    public function setPasswordAttribute($pass){
        $this->attributes['password'] = Hash::make($pass);
    }

    public function getStateAttribute($value)
    {
        switch ($value) {
            case Constants::STATE_ACTIVE:
                return 'ACTIVE';
                break;
            case Constants::STATE_PENDING:
                return 'PENDING';
                break;
            case Constants::STATE_BLOCKED:
                return 'BLOCKED';
                break;
            case Constants::STATE_DELETED:
                return 'DELETED';
                break;
            default:
                return 'Undefined';
        }
    }

    public function addresses()
    {
        return $this->hasMany('App\Address');
    }
}
