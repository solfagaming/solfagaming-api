<?php
namespace App;

use App\Http\Utilities\Constants;
use Illuminate\Database\Eloquent\Model;

class Checkout extends Model
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function address()
    {
        return $this->belongsTo('App\Address');
    }

    public function cart()
    {
        return $this->belongsTo('App\Cart')->with('products');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getCheckoutStateAttribute($value)
    {
        switch ($value) {
            case Constants::CHECKOUT_STATE_CONFIRMED:
                return 'CONFIRMED';
                break;
            case Constants::CHECKOUT_STATE_PENDING_CONFIRMATION:
                return 'PENDING CONFIRMATION';
                break;
            case Constants::CHECKOUT_STATE_REJECTED:
                return 'REJECTED';
                break;
            default:
                return 'Undefined';
        }
    }
}