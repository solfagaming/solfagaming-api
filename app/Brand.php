<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    use Uuids;

    protected $primaryKey = 'brand_id';
    public $incrementing = false;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Create the slug from the name
     */
     public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;
        $this->attributes['slug'] = str_slug($value);
    }
}