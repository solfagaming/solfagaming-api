<?php
namespace App;

use App\Http\Utilities\Constants;
use Illuminate\Database\Eloquent\Model;

class Confirmation extends Model
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}