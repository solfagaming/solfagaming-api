<?php
namespace App;

use App\Http\Utilities\Constants;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function getStateAttribute($value)
    {
        switch ($value) {
            case Constants::STATE_ACTIVE:
                return 'ACTIVE';
                break;
            case Constants::STATE_PENDING:
                return 'PENDING';
                break;
            case Constants::STATE_BLOCKED:
                return 'BLOCKED';
                break;
            case Constants::STATE_DELETED:
                return 'DELETED';
                break;
            default:
                return 'Undefined';
        }
    }
}