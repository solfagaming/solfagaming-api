<?php

namespace App\Mail;

use App\Confirmation;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PaymentConfirmation extends Mailable
{
    use Queueable, SerializesModels;

    public $confirmation;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Confirmation $confirmation)
    {
        $this->confirmation = $confirmation;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Payment Confirmation from ' . $this->confirmation->user->name . '!')
                    ->markdown('emails.checkout.paymentconfirmation');
    }
}