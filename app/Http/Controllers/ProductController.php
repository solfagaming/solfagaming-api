<?php

namespace App\Http\Controllers;

use App\Http\Utilities\Constants;
use Illuminate\Http\Request;
use App\Product;
use App\Category;
use App\Brand;
use Webpatser\Uuid\Uuid;
use Validator;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $products = Product::where('state', Constants::STATE_ACTIVE)->orderBy('name')->get();

        $response['error'] = false;
        $response['message'] = 'Successfully get all products.';
        $response['products'] = $products;
        return response($response, Constants::HTTP_SUCCESS_OK);
    }

    public function detail(Request $request)
    {
        $product = Product::with('category')->where([
            'state' => Constants::STATE_ACTIVE,
            'product_id' => $request->input('product_id')
        ])->first();
        if ($product) {
            $response['error'] = false;
            $response['message'] = 'Successfully get product detail.';
            $response['product'] = $product;
            return response($response, Constants::HTTP_SUCCESS_OK);
        } else {
            $response['error'] = true;
            $response['message'] = 'Product not found.';
            $response['product'] = $product;
            return response($response, Constants::HTTP_ERROR_NOT_FOUND);
        }
    }

    public function search(Request $request)
    {
        $query = Product::query()->with('category');
        if ($request->has('product_id')) {
            $query = $query->where('product_id', $request->input('product_id'));
        }
        if ($request->has('created_by')) {
            $query = $query->where('created_by', $request->input('created_by'));
        }
        if ($request->has('q')) {
            $query = $query->where('name', 'LIKE', '%' . $request->input('q') . '%');
            $query = $query->orWhere('description', 'LIKE', '%' . $request->input('q') . '%');
        }
        if ($request->has('brand_id')) {
            $query = $query->where('brand_id', $request->input('brand_id'));
        }
        if ($request->has('category_id')) {
            $query = $query->where('category_id', $request->input('category_id'));
        }
        if ($request->has('get_all_state') && $request->input('get_all_state') == 1) {
            $query->where('state', '!=', Constants::STATE_DELETED);
        } else {
            $query->where('state', Constants::STATE_ACTIVE);
        }
        if ($request->has('limit')) {
            $page = ($request->has('page')) ? (int)$request->input('page') : 1;
            $offset = ($page - 1) * $request->input('limit');
            $query = $query->offset($offset)->limit($request->input('limit'));
        }
        $query->orderBy('name');
        $products = $query->get();
        $response['error'] = false;
        $response['message'] = 'Successfully get list of products.';
        $response['products'] = $products;
        return response($response, Constants::HTTP_SUCCESS_OK);
    }

    public function create(Request $request)
    {
        $user = $this->getAuthorizedUser($request);
        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'name' => 'required',
            'weight' => 'required|numeric',
            'price' => 'required|numeric',
            'stock' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            $response['error'] = true;
            $response['message'] = 'Validation error.';
            $response['validation'] = $validator->errors();
            return response($response, Constants::HTTP_ERROR_BAD_REQUEST);
        }
        if ($request->has('brand_id')) {
            $brandExist = Brand::where('brand_id', $request->input('brand_id'))->first();
            if (!$brandExist) {
                $response['error'] = true;
                $response['message'] = 'Brand not found.';
                return response($response, Constants::HTTP_SUCCESS_ACCEPTED);
            }
        }
        $categoryExist = Category::where('category_id', $request->input('category_id'))->first();
        if (!$categoryExist) {
            $response['error'] = true;
            $response['message'] = 'Category not found.';
            return response($response, Constants::HTTP_SUCCESS_ACCEPTED);
        }
        //create Product
        $product = new Product;
        $data = $request->all();
        $data['created_by'] = $user->id;
        $product->fill($data);
        if (!$product->save()) {
            $response['error'] = true;
            $response['message'] = 'Error when trying to create product.';
            return response($response, Constants::HTTP_SUCCESS_ACCEPTED);
        } else {
            $response['error'] = false;
            $response['message'] = 'Product has been created.';
            return response($response, Constants::HTTP_SUCCESS_CREATED);
        }
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'category_id' => 'required',
            'name' => 'required',
            'weight' => 'required|numeric',
            'price' => 'required|numeric',
            'stock' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            $response['error'] = true;
            $response['message'] = 'Validation error.';
            $response['validation'] = $validator->errors();
            return response($response, Constants::HTTP_ERROR_BAD_REQUEST);
        }
        $product = Product::where('state', '!=', Constants::STATE_DELETED)
            ->find($request->input('product_id'));
        if (!$product) {
            $response['error'] = true;
            $response['message'] = 'Product not found.';
            return response($response, Constants::HTTP_ERROR_NOT_FOUND);
        }
        $product->fill($request->all());
        if (!$product->save()) {
            $response['error'] = true;
            $response['message'] = 'Error when trying to update product.';
            return response($response, Constants::HTTP_SUCCESS_ACCEPTED);
        }
        $response['error'] = false;
        $response['message'] = 'Product has been updated.';
        return response($response, Constants::HTTP_SUCCESS_OK);
    }

    public function delete(Request $request)
    {
        $user = $this->getAuthorizedUser($request);
        $product = Product::where([
            ['state', '!=', Constants::STATE_DELETED]
        ])->find($request->input('product_id'));
        if (!$product) {
            $response['error'] = true;
            $response['message'] = 'Product not found.';
            return response($response, Constants::HTTP_ERROR_NOT_FOUND);
        }
        $product->state = Constants::STATE_DELETED;
        if (!$product->save()) {
            $response['error'] = true;
            $response['message'] = 'Error when trying to delete product.';
            return response($response, Constants::HTTP_SUCCESS_ACCEPTED);
        }
        $response['error'] = false;
        $response['message'] = 'Product has been deleted.';
        return response($response, Constants::HTTP_SUCCESS_OK);
    }

    /**
     * add a photo to product
     * @param Request $request
     */
    public function addPhoto(Request $request)
    {
        $product = Product::where('state', Constants::STATE_ACTIVE)
            ->find($request->input('product_id'));
        if (!$product) {
            $response['error'] = true;
            $response['message'] = 'Product not found.';
            return response($response, Constants::HTTP_ERROR_NOT_FOUND);
        }
        $existingImages = $product->images == null ? [] : $product->images;
        $newImage = [
            'uuid' => Uuid::generate()->string,
            'title' => $request->input('image_title'),
            'photo' => $request->input('image_photo')
        ];
        array_push($existingImages, $newImage);
        $product->images = $existingImages;
        if (!$product->save()) {
            $response['error'] = true;
            $response['message'] = 'Error when trying to add image into product.';
            return response($response, Constants::HTTP_SUCCESS_ACCEPTED);
        }
        $response['error'] = false;
        $response['message'] = 'Image has been added.';
        $response['image'] = $newImage;
        return response($response, Constants::HTTP_SUCCESS_OK);
    }

    public function deletePhoto(Request $request)
    {
        $product = Product::where('state', '!=', Constants::STATE_DELETED)->find($request->input('product_id'));
        if (!$product) {
            $response['error'] = true;
            $response['message'] = 'Product not found.';
            return response($response, Constants::HTTP_ERROR_NOT_FOUND);
        }
        $existingImages = $product->images == null ? [] : $product->images;
        $imagesCollection = collect($existingImages);
        if (!$imagesCollection->contains('uuid', $request->input('image_uuid'))) {
            $response['error'] = true;
            $response['message'] = 'Image not found. Wrong Uuid.';
            return response($response, Constants::HTTP_ERROR_NOT_FOUND);
        } else {
            $existingImages = $imagesCollection->reject(function ($value, $key) use ($request){
                return $value['uuid'] == $request->input('image_uuid');
            })->values()->all();
            $product->images = $existingImages;
            if (!$product->save()) {
                $response['error'] = true;
                $response['message'] = 'Error when trying to delete product image.';
                return response($response, Constants::HTTP_SUCCESS_ACCEPTED);
            }
            $response['error'] = false;
            $response['message'] = 'Image has been removed.';
            return response($response, Constants::HTTP_SUCCESS_OK);
        }
    }
}
