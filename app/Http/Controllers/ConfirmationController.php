<?php

namespace App\Http\Controllers;

use App\Http\Utilities\Constants;
use Illuminate\Http\Request;

use App\Confirmation;
use DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\PaymentConfirmation;
use Validator;

class ConfirmationController extends Controller
{
    public function create(Request $request)
    {
        $user = $this->getAuthorizedUser($request);
        $validator = Validator::make($request->all(), [
            'invoice_no' => 'required',
            'transfer_date' => 'required',
            'bank_to' => 'required',
            'amount' => 'required|numeric',
            'bank_from' => 'required',
            'notes' => '',
        ]);
        if ($validator->fails()) {
            $response['error'] = true;
            $response['message'] = 'Validation error.';
            $response['validation'] = $validator->errors();
            return response($response, Constants::HTTP_ERROR_BAD_REQUEST);
        }
        DB::beginTransaction();
        //create Confirmation
        $confirmation = new Confirmation;
        $confirmation->fill([
            'user_id' => $user->id,
            'invoice_no' => $request->input('invoice_no'),
            'transfer_date' => $request->input('transfer_date'),
            'bank_to' => $request->input('bank_to'),
            'amount' => $request->input('amount'),
            'bank_from' => $request->input('bank_from'),
            'notes' => $request->input('notes'),
        ]);
        if (!$confirmation->save()) {
            $response['error'] = true;
            $response['message'] = 'Error when trying to create confirmation data.';
            DB::rollBack();
            return response($response, Constants::HTTP_SUCCESS_ACCEPTED);
        } else {
            if (env('APP_ENV') == 'production' || env('APP_ENV') == 'local') {
                try {
                    Mail::to(env('MAIL_FROM_ADDRESS', 'solfagaming@farizal.id'))->send(new PaymentConfirmation($confirmation));
                } catch (\Exception $e) {
                    $response['error'] = true;
                    $response['message'] = 'Gagal mengirim email verifikasi.';
                    $response['log'] = $e->getMessage();
                    DB::rollBack();
                    return response($response, Constants::HTTP_SUCCESS_CREATED);
                }
            }
            $response['error'] = false;
            $response['message'] = 'Confirmation data has been created.';
            DB::commit();
            return response($response, Constants::HTTP_SUCCESS_CREATED);
        }
    }
}