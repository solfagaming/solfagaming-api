<?php
namespace App\Http\Controllers;

use App\Http\Utilities\Constants;
use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    /**
     * index/list all Category
     * @param  Illuminate\Http\Request $request
     * @return Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categories = Category::with(['children' => function ($query) {
            $query->where('state', Constants::STATE_ACTIVE);
        }])->where([
            ['parent_id', '=', '0'],
            ['state', '=', Constants::STATE_ACTIVE],
        ])->orderBy('name')->get();
        $response['error'] = false;
        $response['message'] = 'Successfully get all categories.';
        $response['categories'] = $categories;
        return response($response, Constants::HTTP_SUCCESS_OK);
    }

    public function search(Request $request)
    {
        $query = Category::query();
        if ($request->has('q')) {
            $query = $query->where('name', 'LIKE', '%' . $request->input('q') . '%');
            $query = $query->orWhere('description', 'LIKE', '%' . $request->input('q') . '%');
        }
        if ($request->has('category_id')) {
            $query = $query->where('category_id', $request->input('category_id'));
        }
        if ($request->has('parent_id')) {
            $query = $query->where('parent_id', $request->input('parent_id'));
        }
        if ($request->has('limit')) {
            $query = $query->limit($request->input('limit'));
        }
        if ($request->has('get_parent') && $request->input('get_parent') == 1) {
            $query = $query->with(['parent' => function($q) {
                $q->where('state', Constants::STATE_ACTIVE);
            }]);
        }
        if ($request->has('get_children') && $request->input('get_children') == 1) {
            $query = $query->with(['children' => function($q) {
                $q->where('state', Constants::STATE_ACTIVE);
            }]);
        }
        if ($request->has('get_products') && $request->input('get_products') == 1) {
            $query = $query->with(['products' => function($q) {
                $q->where('state', Constants::STATE_ACTIVE);
            }]);
        }
        if ($request->has('get_all_state') && $request->input('get_all_state') == 1) {
            $query->where('state', '<', Constants::STATE_DELETED);
        } else {
            $query->where('state', Constants::STATE_ACTIVE);
        }
        $query->orderBy('name');
        $categories = $query->get();
        $response['error'] = false;
        $response['message'] = 'Successfully get list of categories.';
        $response['categories'] = $categories;
        return response($response, Constants::HTTP_SUCCESS_OK);
    }

    public function create(Request $request)
    {
        $category = new Category;
        $category->fill($request->all());
        if(!$category->save()) {
            $response['error'] = true;
            $response['message'] = 'Error when trying to create category.';
            return response($response, Constants::HTTP_SUCCESS_ACCEPTED);
        } else {
            $response['error'] = false;
            $response['message'] = 'Category has been created.';
            return response($response, Constants::HTTP_SUCCESS_CREATED);
        }
    }

    public function update(Request $request)
    {
        $category = Category::find($request->input('category_id'));
        if(!$category) {
            $response['error'] = true;
            $response['message'] = 'Category not found.';
            return response($response, Constants::HTTP_ERROR_NOT_FOUND);
        }
        $category->fill($request->all());
        if(!$category->save()) {
            $response['error'] = true;
            $response['message'] = 'Error when trying to update category.';
            return response($response, Constants::HTTP_SUCCESS_ACCEPTED);
        }
        $response['error'] = false;
        $response['message'] = 'Category has been updated.';
        return response($response, Constants::HTTP_SUCCESS_OK);
    }

    public function delete(Request $request)
    {
        $category = Category::where('state', '!=', Constants::STATE_DELETED)
            ->find($request->input('category_id'));
        if(!$category) {
            $response['error'] = true;
            $response['message'] = 'Category not found.';
            return response($response, Constants::HTTP_ERROR_NOT_FOUND);
        }
        $category->state = Constants::STATE_DELETED;
        if(!$category->save()) {
            $response['error'] = true;
            $response['message'] = 'Error when trying to delete category.';
            return response($response, Constants::HTTP_SUCCESS_ACCEPTED);
        }
        $response['error'] = false;
        $response['message'] = 'Category has been deleted.';
        return response($response, Constants::HTTP_SUCCESS_OK);
    }
}