<?php

namespace App\Http\Controllers;

use App\Http\Utilities\Constants;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

use App\Address;
use App\Cart;
use App\Checkout;

class CheckoutController extends Controller
{
    public function index(Request $request)
    {
        $checkouts = Checkout::with('cart', 'address', 'user')->where('user_id', \Auth::user()->id)->get();
        if ($checkouts->isEmpty()) {
            $response['message'] = 'Checkout empty.';
        } else {
            $response['message'] = 'Successfully get user\'s checkouts.';
        }
        $response['error'] = false;
        $response['checkouts'] = $checkouts;
        return response($response, Constants::HTTP_SUCCESS_OK);
    }

    public function create(Request $request)
    {
        $activeCarts = Cart::with('products', 'user')->where([
            ['id', '=', $request->input('cart_id')],
            ['user_id', '=', \Auth::user()->id],
            ['state', '=', Constants::STATE_ACTIVE],
            ['expired_at', '>', Carbon::now()],
            ])->first();
        if (empty($activeCarts)) {
            $response['error'] = true;
            $response['message'] = 'Cart not found or expired.';
            return response($response, Constants::HTTP_ERROR_NOT_FOUND);
        }

        $checkoutAddress = Address::where([
            ['id', '=', $request->input('address_id')],
            ['user_id', '=', \Auth::user()->id],
            ['state', '=', Constants::STATE_ACTIVE]
        ])->first();
        if (empty($checkoutAddress)) {
            $response['error'] = true;
            $response['message'] = 'Checkout address not found.';
            return response($response, Constants::HTTP_ERROR_NOT_FOUND);
        }

        DB::beginTransaction();
        $checkout = new Checkout;
        $checkout->fill([
            'cart_id' => $activeCarts->id,
            'user_id' => \Auth::user()->id,
            'address_id' => $checkoutAddress->id,
            'invoice_no' => self::getNextInvoiceNumber(),
            'delivery_fee' => $request->input('delivery_fee'),
            'subtotal' => $activeCarts->products()->sum(DB::raw('carts_products.price * carts_products.amount')),
            'note' => $request->input('note'),
            'checkout_state' => Constants::CHECKOUT_STATE_PENDING_CONFIRMATION
        ]);
        try{
            if (!$checkout->save()) {
                $response['error'] = true;
                $response['message'] = 'Error when trying to checkout cart.';
                DB::rollback();
                return response($response, Constants::HTTP_SUCCESS_ACCEPTED);
            }
        } catch(\Exception $e){
            $response['error'] = true;
            $response['message'] = 'Error when trying to checkout cart.';
            $response['log'] = $e->getPrevious()->getMessage();
            DB::rollback();
            return response($response, Constants::HTTP_SUCCESS_ACCEPTED);
        }

        $activeCarts->state = Constants::STATE_DELETED;
        try{
            $activeCarts->save();
        } catch(\Exception $e){
            $response['error'] = true;
            $response['message'] = 'Error when trying to deactive cart.';
            $response['log'] = $e->getPrevious()->getMessage();
            DB::rollback();
            return response($response, Constants::HTTP_SUCCESS_ACCEPTED);
        }

        DB::commit();
        $response['error'] = false;
        $response['message'] = 'Cart has been successfully checked out.';

        return response($response, Constants::HTTP_SUCCESS_CREATED);
    }

    public function update(Request $request)
    {
        $checkout = Checkout::where([
            ['id', '=', $request->input('id')],
            ['user_id', '=', \Auth::user()->id],
            ])->first();

        if (empty($checkout)) {
            $response['error'] = true;
            $response['message'] = 'Checkout not found.';
            return response($response, Constants::HTTP_ERROR_NOT_FOUND);
        }

        if ($checkout->checkout_state == Constants::CHECKOUT_STATE_CONFIRMED) {
            $response['error'] = true;
            $response['message'] = 'Cant update checkout. It\'s already confirmed.';
            return response($response, Constants::HTTP_ERROR_NOT_FOUND);
        }

        if ($checkout->checkout_state == Constants::CHECKOUT_STATE_REJECTED) {
            $response['error'] = true;
            $response['message'] = 'Cant update checkout. It\'s has been rejected.';
            return response($response, Constants::HTTP_ERROR_NOT_FOUND);
        }

        $checkout->fill([
            'note' => $request->input('note'),
        ]);
        try{
            $checkout->save();
        } catch(\Exception $e){
            $response['error'] = true;
            $response['message'] = 'Error when trying to update checkout.';
            $response['log'] = $e->getPrevious()->getMessage();
            return response($response, Constants::HTTP_SUCCESS_ACCEPTED);
        }

        $response['error'] = false;
        $response['message'] = 'Checkout has been updated.';
        return response($response, Constants::HTTP_SUCCESS_OK);
    }

    public function reject(Request $request)
    {
        $checkout = Checkout::where('id', $request->input('id'))->first();

        if (empty($checkout)) {
            $response['error'] = true;
            $response['message'] = 'Checkout not found.';
            return response($response, Constants::HTTP_ERROR_NOT_FOUND);
        }

        $checkout->fill([
            'checkout_state' => Constants::CHECKOUT_STATE_REJECTED,
        ]);
        try{
            $checkout->save();
        } catch(\Exception $e){
            $response['error'] = true;
            $response['message'] = 'Error when trying to reject checkout.';
            $response['log'] = $e->getPrevious()->getMessage();
            return response($response, Constants::HTTP_SUCCESS_ACCEPTED);
        }

        $response['error'] = false;
        $response['message'] = 'Checkout has been rejected.';
        return response($response, Constants::HTTP_SUCCESS_OK);
    }

    private static function getNextInvoiceNumber()
    {
        $ymd = date('ymd');
        // Get the last created checkout today
        $numberOfCheckout = Checkout::whereDate('created_at', $ymd)->count();
        // Add the string in front and higher up the number.
        // the %06d part makes sure that there are always 6 numbers in the string.
        // so it adds the missing zero's when needed.;
        $invoice_no = 'INV-' . $ymd . '-' . sprintf('%06d', intval($numberOfCheckout) + 1);
        return $invoice_no;
    }
}