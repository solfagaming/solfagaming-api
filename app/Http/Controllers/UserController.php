<?php
namespace App\Http\Controllers;

use App\Http\Utilities\Constants;
use Illuminate\Http\Request;
use Validator;
use App\User;
use DB;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use App\Mail\UserRegistered;

class UserController extends Controller
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required'
        ]);
        if ($validator->fails()) {
            $response['error'] = true;
            $response['message'] = 'Validation error.';
            $response['validation'] = $validator->errors();
            return response($response, Constants::HTTP_ERROR_BAD_REQUEST);
        }
        DB::beginTransaction();
        //create user
        $user = new User;
        $user->fill($request->except(['role', 'state']));
        $user->email_token = Str::random(40);
        if($user->save()) {
            if (env('APP_ENV') == 'production' || env('APP_ENV') == 'local') {
                try {
                    Mail::to(env('MAIL_FROM_ADDRESS', 'solfagaming@farizal.id'))->send(new UserRegistered($user));
                } catch (\Exception $e) {
                    $response['error'] = true;
                    $response['message'] = 'Gagal mengirim email verifikasi.';
                    $response['log'] = $e->getMessage();
                    DB::rollBack();
                    return response($response, Constants::HTTP_SUCCESS_CREATED);
                }
            }
            $response['error'] = false;
            $response['message'] = 'Sukses registrasi. Silakan cek email untuk aktivasi akun.';
            DB::commit();
            return response($response, Constants::HTTP_SUCCESS_CREATED);
        } else {
            $response['error'] = true;
            $response['message'] = 'Error when trying to create user.';
            DB::rollBack();
            return response($response, Constants::HTTP_SUCCESS_ACCEPTED);
        }
    }

    public function profile(Request $request)
    {
        $user = $this->getAuthorizedUser($request);
        $response['error'] = false;
        $response['message'] = 'Successfully get user profile.';
        $response['user'] = $user;
        $response['user']['addresses'] = $user->addresses;
        return response($response, Constants::HTTP_SUCCESS_OK);
    }

    public function update(Request $request)
    {
        $user = $this->getAuthorizedUser($request);
        $user->fill($request->only(['name', 'steam_url']));
        if ($request->has('password')) {
            $user->password = $request->input('password');
        }
        $user->save();
        $response['error'] = false;
        $response['message'] = 'User profile has been updated.';
        return response($response, Constants::HTTP_SUCCESS_OK);
    }

    public function verifyemail($token)
    {
        $user = User::where('email_token', $token)->first();
        if ($user) {
            $user->state = Constants::STATE_ACTIVE;
            $user->email_token = NULL;
            $user->save();

            $response['error'] = false;
            $response['message'] = 'Konfirmasi email berhasil.';
            return response($response, Constants::HTTP_SUCCESS_OK);
        } else {
            $response['error'] = true;
            $response['message'] = 'Konfirmasi email gagal. Invalid token.';
            return response($response, Constants::HTTP_ERROR_NOT_FOUND);
        }
    }
}
