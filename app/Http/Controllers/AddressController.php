<?php

namespace App\Http\Controllers;

use App\Http\Utilities\Constants;
use Illuminate\Http\Request;

use App\Address;

class AddressController extends Controller
{
    public function index(Request $request)
    {
        $addresses = \Auth::user()->addresses()->where('state', Constants::STATE_ACTIVE)->get();
        if ($addresses->isEmpty()) {
            $response['message'] = 'Address empty.';
        } else {
            $response['message'] = 'Successfully get user\'s addresses.';
        }
        $response['error'] = false;
        $response['addresses'] = $addresses;
        return response($response, Constants::HTTP_SUCCESS_OK);
    }

    public function create(Request $request)
    {
        $user = $this->getAuthorizedUser($request);
        $address = new Address;
        $address->user_id = $user->id;
        $address->fill([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'phone' => $request->input('phone'),
            'address' => $request->input('address'),
            'subdistrict_id' => $request->input('subdistrict_id')
        ]);
        try{
            if (!$address->save()) {
                $response['error'] = true;
                $response['message'] = 'Error when trying to create address.';
                return response($response, Constants::HTTP_SUCCESS_ACCEPTED);
            }
        } catch(\Exception $e){
            $response['error'] = true;
            $response['message'] = 'Error when trying to create address.';
            $response['log'] = $e->getPrevious()->getMessage();
            return response($response, Constants::HTTP_SUCCESS_ACCEPTED);
        }

        $response['error'] = false;
        $response['message'] = 'Address has been created.';
        return response($response, Constants::HTTP_SUCCESS_CREATED);
    }

    public function update(Request $request)
    {
        $user = $this->getAuthorizedUser($request);
        $address = Address::where([
            ['id', '=', $request->input('id')],
            ['user_id', '=', $user->id],
            ['state', '=', Constants::STATE_ACTIVE]
        ])->first();
        if (!$address) {
            $response['error'] = true;
            $response['message'] = 'Address not found.';
            return response($response, Constants::HTTP_ERROR_NOT_FOUND);
        }
        $address->fill($request->all());
        if (!$address->save()) {
            $response['error'] = true;
            $response['message'] = 'Error when trying to update address.';
            return response($response, Constants::HTTP_SUCCESS_ACCEPTED);
        }

        $response['error'] = false;
        $response['message'] = 'Address has been updated.';
        return response($response, Constants::HTTP_SUCCESS_OK);
    }

    public function delete(Request $request)
    {
        $user = $this->getAuthorizedUser($request);
        $address = Address::where([
            ['id', '=', $request->input('id')],
            ['user_id', '=', $user->id],
            ['state', '!=', Constants::STATE_DELETED]
        ])->first();
        if (!$address) {
            $response['error'] = true;
            $response['message'] = 'Address not found.';
            return response($response, Constants::HTTP_ERROR_NOT_FOUND);
        }
        $address->state = Constants::STATE_DELETED;
        if (!$address->save()) {
            $response['error'] = true;
            $response['message'] = 'Error when trying to delete address.';
            return response($response, Constants::HTTP_SUCCESS_ACCEPTED);
        }
        $response['error'] = false;
        $response['message'] = 'Address has been deleted.';
        return response($response, Constants::HTTP_SUCCESS_OK);
    }
}