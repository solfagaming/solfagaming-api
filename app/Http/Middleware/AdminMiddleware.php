<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Utilities\Constants;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user_role = $request->get('user_role');
        if($user_role != Constants::USER_ROLE_ADMIN) {
            $response['error'] = true;
            $response['message'] = 'Unauthorized access.';
            return response($response, Constants::HTTP_ERROR_UNAUTHORIZED);
        }
        return $next($request);
    }
}
