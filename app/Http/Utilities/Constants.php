<?php

namespace App\Http\Utilities;

class Constants
{
    const STORE_EMAIL_INFO = 'cs@solfagaming.com';
    const STORE_EMAIl_NAME = 'CS Solfagaming';

    // User constants
    const USER_ROLE_ADMIN = 1;
    const USER_ROLE_CUSTOMER = 2;

    // State constants
    const STATE_ACTIVE = 1;
    const STATE_PENDING = 2;
    const STATE_BLOCKED = 3;
    const STATE_DELETED = 4;

    // HTTP status code
    const HTTP_SUCCESS_OK = 200;
    const HTTP_SUCCESS_CREATED = 201;
    const HTTP_SUCCESS_ACCEPTED = 202;
    const HTTP_SUCCESS_NO_CONTENT = 204;
    const HTTP_ERROR_BAD_REQUEST = 400;
    const HTTP_ERROR_UNAUTHORIZED = 401;
    const HTTP_ERROR_FORBIDDEN = 403;
    const HTTP_ERROR_NOT_FOUND = 404;
    const HTTP_ERROR_CONFLICT = 409;
    const HTTP_ERROR_GONE = 410;

    // Checkout state constants
    const CHECKOUT_STATE_CONFIRMED = 1;
    const CHECKOUT_STATE_PENDING_CONFIRMATION = 2;
    const CHECKOUT_STATE_REJECTED = 3;
    // Doku payment status constants
    const DOKU_PAYMENT_FAILED = 1;
    const DOKU_PAYMENT_SUCCESS = 2;
    const DOKU_PAYMENT_PENDING = 3;

    // Delivery constants
    const DELIVERY_DELIVERED = 1;
    const DELIVERY_CANCELED = 2;
    const DELIVERY_FINDING_COURIER = 3;
    const DELIVERY_ON_PROGRESS = 4;
    const DELIVERY_ERROR = 5;
    // Date format constants
    const FORMAT_DATE_NORMAL = 'Y-m-d H:i:s';
}
