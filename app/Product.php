<?php
namespace App;

use App\Http\Utilities\Constants;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use Uuids;

    protected $primaryKey = 'product_id';
    public $incrementing = false;

    protected $guarded = [
        'product_id'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'options' => 'array',
        'attributes' => 'array',
        'images' => 'array',
    ];

    public function getStateAttribute($value)
    {
        switch ($value) {
            case Constants::STATE_ACTIVE:
                return 'ACTIVE';
                break;
            case Constants::STATE_PENDING:
                return 'PENDING';
                break;
            case Constants::STATE_BLOCKED:
                return 'BLOCKED';
                break;
            case Constants::STATE_DELETED:
                return 'DELETED';
                break;
            default:
                return 'Undefined';
        }
    }

    public function category()
    {
        return $this->belongsTo('App\Category', 'category_id');
    }
}