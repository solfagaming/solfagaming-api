@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => 'javascript:void(0)'])
            {{ env('APP_NAME', 'Laravel') }}
        @endcomponent
    @endslot

{{-- Body --}}
# Wellcome {{ $user->name }}!

Thank you for registering on {{ env('APP_NAME', 'Laravel') }}!

Please verify Your Email Address by clicking the button below.

@component('mail::button', ['url' => url('v1/verifyemail/' . $user->email_token), 'color' => 'blue'])
Confirm My Account
@endcomponent

Thanks,

{{ env('MAIL_FROM_NAME', 'Laravel Admin') }}
{{-- End Body --}}

    {{-- Subcopy --}}
    @if (isset($subcopy))
        @slot('subcopy')
            @component('mail::subcopy')
                {{ $subcopy }}
            @endcomponent
        @endslot
    @endif

    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            © {{ date('Y') }} {{ env('APP_NAME', 'Laravel') }}. All rights reserved.
        @endcomponent
    @endslot
@endcomponent
