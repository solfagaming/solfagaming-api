@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => 'javascript:void(0)'])
            {{ env('APP_NAME', 'Laravel') }}
        @endcomponent
    @endslot

{{-- Body --}}
# Payment Confirmation from {{ $confirmation->user->name }}!

@component('mail::table')
| #1       | #2         |
| ------------- |---------------|
| Customer      | {{ $confirmation->user->name }} |
| Invoice No    | {{ $confirmation->invoice_no }} |
| Transfer Date | {{ $confirmation->transfer_date }} |
| Transfer to   | {{ $confirmation->bank_to }} |
| Amount        | {{ $confirmation->amount }} |
| Transfer From | {{ $confirmation->bank_from }} |
| Notes         | {{ $confirmation->notes }} |
@endcomponent
{{-- End Body --}}

    {{-- Subcopy --}}
    @if (isset($subcopy))
        @slot('subcopy')
            @component('mail::subcopy')
                {{ $subcopy }}
            @endcomponent
        @endslot
    @endif

    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            © {{ date('Y') }} {{ env('APP_NAME', 'Laravel') }}. All rights reserved.
        @endcomponent
    @endslot
@endcomponent
