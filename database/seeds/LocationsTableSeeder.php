<?php

use Illuminate\Database\Seeder;

class LocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Eloquent::unguard();
        $path = 'storage/location_database.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Location table seeded!');
    }
}
