<?php

use Illuminate\Database\Seeder;
use Webpatser\Uuid\Uuid;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cat1 = factory(App\Category::class)->create([
            'name' => 'Test category 1'
        ]);

        $subcat1 = factory(App\Category::class)->create([
            'parent_id' => $cat1->category_id,
            'name' => 'Test sub category 1'
        ]);

        $cat2 = factory(App\Category::class)->create([
            'name' => 'Test category 2'
        ]);

        $subcat2 = factory(App\Category::class)->create([
            'parent_id' => $cat2->category_id,
            'name' => 'Test sub category 2'
        ]);


        // factory(App\Category::class)->create([
        //     'name' => 'Test category 2'
        // ])->each(function ($c) {
        //         factory(App\Category::class)->create([
        //             'parent_id' => $c->category_id,
        //             'name' => 'Test sub category 2'
        //         ]);
        // });
        //
        // factory(App\Category::class)->create([
        //     'name' => 'Test category 1'
        // ])->each(function ($c) {
        //         factory(App\Category::class)->create([
        //             'parent_id' => $c->category_id,
        //             'name' => 'Test sub category 1'
        //         ]);
        // });
    }
}
