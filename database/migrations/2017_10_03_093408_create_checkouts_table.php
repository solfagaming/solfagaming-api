<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCheckoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checkouts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cart_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('address_id')->unsigned();
            $table->string('invoice_no');
            $table->double('delivery_fee', 19, 2)->nullable();
            $table->double('subtotal', 19, 2);
            $table->string('note')->nullable();
            $table->smallInteger('checkout_state')->default(2);
            $table->timestamps();

            $table->foreign('cart_id')
                ->references('id')->on('carts')
                ->onDelete('cascade');
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('address_id')
                ->references('id')->on('addresses')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('checkouts');
    }
}
