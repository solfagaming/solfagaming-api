<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function(Blueprint $table){
            $table->increments('id');
            $table->string('facebook_id');
            $table->string('first_name', 50);
            $table->string('last_name', 50);
            $table->string('name', 50);
            $table->string('email')->unique();
            $table->string('password');
            $table->string('steam_url');
            $table->string('email_token', 64)->nullable();
            $table->smallInteger('role')->default(2);
            $table->smallInteger('state')->default(2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
