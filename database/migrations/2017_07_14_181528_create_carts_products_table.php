<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartsProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carts_products', function(Blueprint $table){
            $table->integer('cart_id')->unsigned();
            $table->foreign('cart_id')
                ->references('id')->on('carts')
                ->onDelete('cascade');

            $table->char('product_id', 36);
            $table->foreign('product_id')
                ->references('product_id')->on('products')
                ->onDelete('cascade');

            $table->smallInteger('amount');
            $table->double('price', 19, 2);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('carts_products');
    }
}
